// Copyright (C) 2009 - Michael Baudin
// This file must be used under the terms of the GNU LGPL license.

// 
// probabilitydisease --
//   Returns the probability that a person has the 
//   disease, given that the test is positive.
// Arguments 
//   ped: the probability of a positive test, given that the person has the disease
//   pd : the probability of the disease
//   pedc : the probability of a positive test, given that the person has not the disease
//  
function pde = probabilitydisease ( ped , pd , pedc )
  pde = ( ped * pd ) / ( ped * pd + pedc * ( 1. - pd ) )
endfunction

probabilitydisease ( ped = 0.99 , pd = 0.005 , pedc = 0.01 )
// Table of the probability of having the disease, with varying pedc,
// that is with more or less reliable test.
for pedc = [0.1 0.05 0.01 0.005 0.001 0.0005 0.0001]
  pde = probabilitydisease ( ped = 0.99 , pd = 0.005 , pedc);
  mprintf("%f & %f \\\\\n",pedc,pde);
end

// Table of the probability of having the disease, with varying pd,
// that is with variable disease probability.
for pd = [0.5 0.1 0.05 0.01 0.005 0.001 0.0005 0.0001]
  pde = probabilitydisease ( ped = 0.99 , pd , pedc = 0.01);
  mprintf("%f & %f \\\\\n",pd,pde);
end

