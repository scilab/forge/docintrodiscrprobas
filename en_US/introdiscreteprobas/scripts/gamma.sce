// Copyright (C) 2009 - Michael Baudin
// This file must be used under the terms of the GNU LGPL license.

x = linspace ( -4 , 4 , 1001 );
y = gamma ( x );
plot ( x , y );
h = gcf();
h.children.data_bounds = [
  - 4.  -6
    4.   6
];

