// Copyright (C) 2009 - Michael Baudin
// This file must be used under the terms of the GNU LGPL license.

// A fair coin is tossed six times.
// What is the probability that exactly 3 heads turn up ?
n = 6; pb = 0.5; 
p = binopdf_naive ( 3 , n , pb  ) // p = 0.3125

// A puffin factory produces 200 puffins each day.
// The probability of producing a defective puffin is 2%.
// What is the probability that exactly 0 puffins are produced ?
n = 200; pb = 2/100; 
p = binopdf_naive ( 0 , n , pb  ) // p = 0.0175879

// Test by Yalta
// Yalta tests the CDF, I test the PDF.
n = 1030; pb = 0.5; qb = 0.5;
// http://www.wolframalpha.com :
// nchoosek(1030,1) * 0.5^1 * (1-0.5)^(1030-1)
p = binopdf ( 1 , n , pb , qb ) // Exact = 8.95244560258756806... x 10^-308

// Test by me.
// Assume that p = 1 - 1.e-20, i.e. there is an extremely strong probability of success.
// Assume that we make 100 trials.
// What is the probability of having 90 sucesses ?
n = 100; pb = 1 - 1.e-20; qb = 1.e-20;
// http://www.wolframalpha.com :
// nchoosek(100,90) * (1-10^-20)^90 * (10^-20)^(100-90)
p1 = binopdf ( 90 , n , pb , qb ) // Exact = 1.73103094564399999... x 10^-187
p2 = binopdf_naive ( 90 , n , pb )

// A less obvious case.
// Less naive is still naive !
// nchoosek(10^9,10^9-48) * (1-10^-14)^(10^9-48) * (10^-14)^48
n = 1.e9; pb = 1 - 1.e-14; qb = 1.e-14;
x = 1.e9 - 48
binopdf_lessnaive ( x , n , pb , qb )
// Exact result ( Wolfram Alpha )
e = 8.05538642991600721e-302
// What happens ?
nchoosek(n,x) 
pb^x
qb^(n-x)
// Solution : the logarithm, once again.
p = binopdf ( x , n , pb , qb )
d = -log10(abs(p-e)/e)
// What is the condition number there ?
p1 = binopdf ( x + 2 * %eps * x , n , pb , qb )
ry = abs(p2 - p)/p
rx = 2*%eps
c = ry/rx

function [ y , c , rx , ry ] = myelmfuncond ( x , f )
  // Computes the empirical condition number of the function f at point x.
  // Parameters
  //   x : a floating point number, the current point
  //   f : a function with header y = f(x)
  //   y : the computed value of f at point x
  //   rx : the relative change of x
  //   ry : the relative change of y
  //   c : the condition number as the ratio ry/rx

  d = x * %eps
  y = f(x)
  y1 = f(x+d)
  rx = d/x
  ry = abs(y1-y)/abs(y)
  c = ry / rx
endfunction

function y = mybinopdf ( x )
  n = 1.e9; pb = 1 - 1.e-14; qb = 1.e-14;
  y = binopdf ( x , n , pb , qb )
endfunction

x = 1.e9 - 1;
[ y , c , rx , ry ] = myelmfuncond ( x , mybinopdf )

x = linspace ( 0 , 1.e9 , 1000 );
y = mybinopdf ( x );
plot ( x , y , "b*" )


