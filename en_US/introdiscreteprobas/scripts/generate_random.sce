// Copyright (C) 2009 - Michael Baudin
// This file must be used under the terms of the GNU LGPL license.

// Startup the random sequence
function random_startup ( seed )
  global random_previous
  random_previous = seed
endfunction 
// Generates a uniform sequence based on linear congruential
// random number generator
function u = random_generate ( a , c , m )
  global random_previous
  random_previous = modulo ( a * random_previous + c , m )
  disp(random_previous)
  u = random_previous / m
endfunction 
// Generates the uniform sequence from the book
random_startup ( 7 )
a = 7;
c = 7;
m = 10;
u = zeros(1,8);
for i = 1:8
  u(i) = random_generate ( a , c , m );
end
u
// Startup the 32 bits random sequence
function random_startup32 ( seed )
  global random_previous
  random_previous = seed
endfunction 
// Generates a uniform sequence based on linear congruential
// random number generator
// Based on 32-bits integers
// TODO : get a closer version of Scilab's rand.
function u = random_generate32 ( a , c , m )
  global random_previous
  random_previous = int32 ( modulo ( a * random_previous + c , m ) )
  if ( random_previous < 0 ) then
    random_previous = random_previous + 2^32
  end
  disp(random_previous)
  u = double ( random_previous ) / m
endfunction 
// Generates the sequence of Scilab
random_startup32 ( 0 )
a = 843314861;
c = 453816693;
m = 2^31;
u = zeros(1,8);
for i = 1:8
  u(i) = random_generate32 ( a , c , m );
end
u
// Compare with Scilab's rand
rand("seed",0)
rand(1,8)


