// Copyright (C) 2009 - Michael Baudin
// This file must be used under the terms of the GNU LGPL license.






// Compute (n,j) for various values of n and j.
for n=0:5
  for j=0:n
    c = nchoosek ( n , j );
    mprintf("(%d,%d)=%2d   ",n,j,c);
  end
  mprintf("\n");
end

// Now try (4,1) and check that rounding to the nearest integer 
// is necessary.
format(20);
n = 4;
j = 1;
c = exp(gammaln(n+1)-gammaln(j+1)-gammaln(n-j+1))

// Probability of getting exactly 3 heads in 6 toss of a fair coin.
p = 1/2
q = 1-p
n = 6
j = 3
bnpj = nchoosek(n,j) * p^j * q^(n-j)
// Probability of getting exactly one "6" in 4 toss of a fair die.
p = 1/6
q = 1-p
n = 4
j = 1
bnpj = nchoosek(n,j) * p^j * q^(n-j)

// one6inNtoss --
//   Computes the probability of getting exactly one "6" in n toss of a fair die.
function bnpj = one6inNtoss ( n )
  p = 1/6
  q = 1-p
  j = 1
  bnpj = nchoosek(n,j) * p^j * q^(n-j)
endfunction
for n = 1:12
  b = one6inNtoss ( n );
  mprintf("In %d toss, p(one six)=%f\n",n,b);
end


