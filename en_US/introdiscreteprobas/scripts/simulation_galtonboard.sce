// Copyright (C) 2009 - Michael Baudin
// This file must be used under the terms of the GNU LGPL license.

// simulgalton --
//   Performs one simulation of the Galton board with n stages, 
//   and returns the index j=1,2,...,n where the ball falls. --
function j = simulgalton ( n , verbose )
  if exists("verbose","local")==0 then
    verbose = 0
  end
  jmin = 1
  for k = 1 : n
    if verbose == 1 then
      mprintf("Step #%d (%d)\n",k,jmin)
    end
    r = rand()
    if r<0.5 then
      if verbose == 1 then
        mprintf("To the left !\n")
      end
    else
      if verbose == 1 then
        mprintf("To the right !\n")
      end
      jmin = jmin + 1
    end
  end
  j = jmin
endfunction

// Perform nshots experiments of the Galton board.
rand ( "seed" , 0 )
n = 10
cups = zeros(1,n+1)
nshots = 10000
for k = 1:nshots
  j = simulgalton ( n );
  cups(j) = cups(j) + 1;
end
cups = cups / nshots

bar(1:n+1, cups)
//xs2png(0,"simulation_galtonboard.png")
//clf()
// Plot exact probabilities
pr = binomial(0.5,n)
//bar(1:n+1, pr)
plot(1:n+1, pr)

// Put dots on the exact binomial
h = gcf();
h.children.children(1).children.mark_mode = "on";
h.children.children(1).children.mark_size = 2;

legend ( [ "Galton board" , "Binomial distribution" ] )
title ( msprintf ("Simulation of the Galton board with n=%d" , nshots ) )
//xs2png(0,"binomial_distribution.png")
//clf()

// Generate random numbers from the binomial distribution
d = grand(1000,1,"bin", n, 0.5);
histplot(n+1,d)

//---------------------------------------------------------------
// Dynamic update of the Galton board

rand ( "seed" , 1 )
n = 10
cups = zeros(1,n+1)
drawlater();
bar(1:n+1, cups)
hh = gcf();
nshots = 200;
cmaxdefault = 30;
hh.children.data_bounds = [
  0 0
  n+2 cmaxdefault
  ];
drawnow();
for k = 1:nshots
  j = simulgalton ( n );
  cups(j) = cups(j) + 1;
  hh.children.children.children.data(:,2) = cups';
  cmax = 1.1 * max(max(cups),cmaxdefault);
  hh.children.data_bounds = [
      0 0
      n+2 cmax
    ];
  xtitle ( msprintf ("Run #: %d, cups= [%s]", k , strcat ( string ( cups ) , " " ) ) );
end

