// Copyright (C) 2009 - Michael Baudin
// This file must be used under the terms of the GNU LGPL license.



// The total number of hands of 5 cards in a 52 cards deck = 2598960.
total = nchoosek(52,5)
// Probability of a "four of a kind"
p4 = 13*48/total
// Probability of a "full house" = 0.0014406
pfull = 13*12*nchoosek(4,2)*nchoosek(4,3)/total
// Probability of a "pair" = 0.4225690
ppair = 13*nchoosek(4,2)*nchoosek(12,3) * 4^3/total
// Probability of a "double pair" = 0.0475390
pdoublepair = nchoosek(13,2)*nchoosek(4,2)^2*11*4/total
// Probability of a "three of a kind"
pthree = 13 * nchoosek(4,3) * nchoosek(12,2) * 4^2/total
// Probability of a "straight" (excluding straight flush and royal flush) =  0.0039246
pstraight = (10*4^5 - 4*10)/total
// Probability of a "flush" (excluding straight flush and royal flush) = 0.0019654 
pflush = (4*nchoosek(13,5)- 4*10)/total
// Probability of a "straight flush" (excluding "royal flush") = 0.0000139
pstraightflush = (4*10 - 4)/total
// Probability of a "royal flush" = 0.0000015
proyalflush = 4/total
// Probability of "no pair"
pnopair = 1 ...
- ppair ...
- pdoublepair ...
- pthree ...
- pstraight ...
- pflush ...
- pfull ...
- p4 ...
- pstraightflush ...
- proyalflush

