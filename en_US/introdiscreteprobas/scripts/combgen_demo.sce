// Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin
// This file must be used under the terms of the GNU LGPL license.



x = [1 2 3 4];
k = 3;
cmap = combgen ( x , k )
//     1.    2.    3.  
//     1.    2.    4.  
//     1.    3.    4.  
//     2.    3.    4.  



x = [17 32 48 53];
k = 3;
cmap = combgen ( x , k )
//     17.    32.    48.  
//     17.    32.    53.  
//     17.    48.    53.  
//     32.    48.    53.  


x = [17 32 48 53 72];
k = 3;
cmap = combgen ( x , k )
//     17.    32.    48.  
//     17.    32.    53.  
//     17.    32.    72.  
//     17.    48.    53.  
//     17.    48.    72.  
//     17.    53.    72.  
//     32.    48.    53.  
//     32.    48.    72.  
//     32.    53.    72.  
//     48.    53.    72.  

x = [17 32 48 53 72];
k = 4;
cmap = combgen ( x , k )
//    17.    32.    48.    53.  
//    17.    32.    48.    72.  
//    17.    32.    53.    72.  
//    17.    48.    53.    72.  
//    32.    48.    53.    72.  

combgen(["a" "b" "c" "d" "e" "f"],4)




// test various algorithms
E1 = subsets(1:10,3);
E2 = combgen(1:10,3);
E3 = nchoosek_re(1:10,3);
size(E1)
size(E2)
size(E3)
and(E1 == E2)
and(E2 == E3)

// Bench various algorithms
n = 60
k = 3
nchoosek(n,k) // 34220
benchfun ( "subsets" , subsets , list(1:n,k) , 1 , 10 );
benchfun ( "combgen" , combgen , list(1:n,k) , 1 , 10 );
benchfun ( "nchoosek_re" , nchoosek_re , list(1:n,k) , 1 , 10 );
// nchoosek_re: 10 iterations, mean=0.121681, min=0.109201, max=0.140401
// subsets:     10 iterations, mean=0.185641, min=0.171601, max=0.202801
// combgen:     10 iterations, mean=0.308882, min=0.296402, max=0.312002


n = 18
k = 9
nchoosek(n,k) // 48620
benchfun ( "subsets" , subsets , list(1:n,k) , 1 , 10 );
benchfun ( "combgen" , combgen , list(1:n,k) , 1 , 10 );
benchfun ( "nchoosek_re" , nchoosek_re , list(1:n,k) , 1 , 10 );
// combgen:     10 iterations, mean=0.472683, min=0.421203, max=0.514803
// subsets:     10 iterations, mean=1.770611, min=1.716011, max=1.887612
// nchoosek_re: 10 iterations, mean=2.850138, min=2.823618, max=2.870418

// Make the recursive version fail
n = 10000
k = n
combgen(1:n,k);
nchoosek_re (1:n,k);
subsets (1:n,k);

// Bench various algorithms
n = 200
k = 2
nchoosek(n,k) // 34220
benchfun ( "subsets" , subsets , list(1:n,k) , 1 , 10 );
benchfun ( "combgen" , combgen , list(1:n,k) , 1 , 10 );
benchfun ( "nchoosek_re" , nchoosek_re , list(1:n,k) , 1 , 10 );
// nchoosek_re: 10 iterations, mean=0.121681, min=0.109201, max=0.140401
// subsets:     10 iterations, mean=0.185641, min=0.171601, max=0.202801
// combgen:     10 iterations, mean=0.308882, min=0.296402, max=0.312002


