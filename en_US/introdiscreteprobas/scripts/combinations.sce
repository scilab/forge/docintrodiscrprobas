// Copyright (C) 2009 - Michael Baudin
// This file must be used under the terms of the GNU LGPL license.



// Compute (n,j) for various values of n and j.
for n=0:5
  for j=0:n
    c = nchoosek ( n , j );
    mprintf("(%d,%d)=%2d   ",n,j,c);
  end
  mprintf("\n");
end

for n=0:5
  for j=0:n
    c = nchoosek ( n , j );
    mprintf("%2d   ",c);
  end
  mprintf("\n");
end

// Now try (4,1) and check that rounding to the nearest integer 
// is necessary.
format(20);
n = 4;
j = 1;
c = exp(gammaln(n+1)-gammaln(j+1)-gammaln(n-j+1))

nchoosek ( 4 , 1 )

n = 5 * ones(6,1);
j = (0:5)';
c = nchoosek ( n , j );
[n j c]




