// Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

function cmap = combgen ( x , k )
  // Generate all combinations of k values from x.
  // 
  // Calling Sequence
  //   cmap = combgen ( x , k )
  //
  // Parameters
  //   x : a matrix of doubles, the values to be combined
  //   k : a 1 x 1 matrix of floating point integers, the number of elements in each combination
  //   cmap : a cnk x k matrix of doubles, the combinations. Here cnk is equal to the number of combinations of n elements from k. Each row of cmap is a combination.
  //
  // Description
  // Generate all combinations as a row-by-row array 
  // with (n,k) rows where (n,k) is the binomial coefficient and n is the 
  // number of values in x. 
  // If i is an integer from 1 to cnk, the row vector cmap(i,1:k) is a particular combination.
  // Notice that the combinations are computed without replacement.
  // Use combinatevectors if all permutations are to be generated.
  //
  // Note
  //
  // The algorithm proceeds as following.
  // The number of nchoosek is computed from c = nchoosek ( n , k )
  // For example, with n=5, k=3, we get c=10.
  // The first possible combination is a = 1:k.
  // For example, with n=5 and k = 3, the first combination is 
  // a = [1 2 3].
  // We store in cmap the value of x(a), that is, the first row of 
  // cmap is [x(1) x(2) x(3)].
  // Then a loop is performed for all remaining nchoosek, from 2 to c.
  // The nchoosek are enumerated in order.
  // For example, with n=5, k=3, we compute the remaining nchoosek in the 
  // following order :
  // a=[1 2 4]
  // a=[1 2 5]
  // a=[1 3 4]
  // a=[1 3 5]
  // a=[1 4 5]
  // a=[2 3 4]
  // a=[2 3 5]
  // a=[2 4 5]
  // a=[3 4 5]
  // Once the combination vector a is computed, we store 
  // x(a) in the corresponding row of the matrix cmap.
  // Let us detail how the nchoosek are computed, that 
  // is, how the vector a is updated.
  // First, we search for i, the first digit to be updated.
  // We use a backward search, starting from i = k and look for the 
  // condition a(i) == n - k + i. If that condition is satisfied,
  // we continue the search and decrease i. If not, we stop.
  // For example, if a=[1 3 4], we find i = 3 (because a(3)=4
  // is different from n=5) and if a=[1 4 5], we find i=1
  // (because a(3)=5=n and a(2)=4=n-1).
  // Then we increase a(i) and update all the integers  
  // a(i+1), a(i+2), ..., a(k).
  // This procedure is as vectorized as possible : I guess 
  // that more speed will require to write compiled source code.
  //
  // Examples
  //   combgen ( 1:4 , 3 )
  //   combgen ( [17 32 48 53] , 3 )
  //   combgen ( [17 32 48 53 72] , 3 )
  //
  // Authors
  //   2009 - DIGITEO - Michael Baudin
  //
  // Bibliography
  // http://home.att.net/~srschmitt/script_nchoosek.html
  // Kenneth H. Rosen, Discrete Mathematics and Its Applications, 2nd edition (NY: McGraw-Hill, 1991), pp. 284-286.
  
  
  n = size(x,"*")
  if ( ( k < 0 ) | ( k > n ) ) then 
    nstr = strcat(string(n)," ")
    kstr = strcat(string(k)," ")
    errmsg = msprintf ( gettext ( "%s: The parameters n = %s and k = %s are not consistent." ) , ...
    "combgen" , nstr , kstr )
    error ( errmsg )
  end
  // If the input are not floating point integers, generates an error
  if ( or(round(k)<>k) ) then
    nstr = strcat(string(n)," ")
    kstr = strcat(string(k)," ")
    errmsg = msprintf ( gettext ( "%s: The parameters n = %s and k = %s are not floating point integers." ) , ...
    "combgen" , nstr , kstr )
    error ( errmsg )
  end
  c = nchoosek ( n , k )
  cmap = zeros(c,k)
  a = 1:k
  cmap(1,:) = x(a)
  for m = 2 : c
    r = k : -1 : 1
    i = k - find ( a(r) <> n - k + r , 1 ) + 1
    a(i) = a(i) + 1
    j = i+1 : k
    a(j) = a(i) + j - i
    cmap(m,:) = x(a)
  end
endfunction


// Returns all nchoosek of k values from x as a row-by-row array
// with (n,k) rows where (n,k) is the binomial coefficient and n is the 
// number of values in x. 
// http://home.att.net/~srschmitt/script_nchoosek.html
// Kenneth H. Rosen, Discrete Mathematics and Its Applications, 2nd edition (NY: McGraw-Hill, 1991), pp. 284-286.
function cmap = combgen_naive ( x , k )
  n = size(x,"*")
  c = nchoosek ( n , k )
  cmap = zeros(c,k)
  a = 1:k
  cmap(1,:) = x(a)
  for m = 2 : c
    i = k
    while ( a(i) == n - k + i )
      i = i - 1
    end
    a(i) = a(i) + 1
    for j = i+1 : k
      a(j) = a(i) + j - i
    end
    cmap(m,:) = x(a)
  end
endfunction

function F = combgen_recursive ( E , m )
   // Authors : Chancelier, Pincon
  // compute all the subsets with m elements of the set E
  // (E must be given as a row vector and all its elements
  // must be differents). For sets of numbers.
  n = length(E)
  if m > n then
    F = []
  elseif m == n
    F = E
  elseif m == 1
    F = E';
  else
    F = [];
    for i = 1:n-m+1
      EE = E(i+1:n)
      FF = combgen_recursive(EE,m-1)
      mm = size(FF,1)
      F = [F ; E(i)*ones(mm,1),FF]
    end
  end
endfunction

