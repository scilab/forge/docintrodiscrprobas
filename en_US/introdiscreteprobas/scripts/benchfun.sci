// Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [t,msg] = benchfun ( name , __benchfun_fun__ , iargs , nlhs , kmax )
  // Benchmarks a function and measure its performance.
  //
  // Calling sequence
  //   [t,msg] = benchfun ( name , fun , iargs , nlhs , kmax )
  //
  // Parameters
  //   name : a 1 x 1 matrix of strings, the name of the function to be executed
  //   fun : a function, the function to be executed
  //   iargs : a list, the list of input arguments for the function
  //   nlhs : a 1 x 1 matrix of floating point integers, the number of output arguments of the function
  //   kmax : a 1 x 1 matrix of floating point integers, the number of executions
  //   t : a kmax x 1 matrix of doubles, the system times, in seconds, required to execute the function
  //   msg : a 1 x 1 matrix of strings, a message summarizing the benchmark
  //
  // Description
  //   This function is designed to be used when measuring the 
  //   performance of a function.
  //   It uses the timer function to measure the system time.
  //   The function is executed kmax times and the performance is 
  //   gathered into the matrix t.
  //   The message summarizes the test and contains the mean,
  //   min and max of the times.
  //
  // Examples
  // function c = pascalup_col (n)
  //   // Pascal up matrix.
  //   // Column by column version
  //   c = eye(n,n)
  //   c(1,:) = ones(1,n)
  //   for i = 2:(n-1)
  //     c(2:i,i+1) = c(1:(i-1),i)+c(2:i,i)
  //   end
  // endfunction
  // benchfun ( "pascalup_col" , pascalup_col , 100 , 10 );
  //
  // Authors
  //   2010 - DIGITEO - Michael Baudin
  
  [lhs,rhs] = argn()
  if ( rhs<>5 ) then
    noerrmsg = sprintf(gettext("%s: Unexpected number of arguments : %d provided while %d are expected."),..
      "benchfun",rhs,5);
    error(noerrmsg)
  end
  //
  // Check type
  if ( typeof(name) <> "string" ) then 
    error(sprintf(gettext("%s: Wrong type for input argument #%d: variable %s has type %s while %s is expected.\n"), ..
      "benchfun", 1, "name" , typeof(name) , "string"))
  end
  if ( and ( typeof(__benchfun_fun__) <> ["function" "list" "fptr"] ) ) then 
    error(sprintf(gettext("%s: Wrong type for input argument #%d: variable %s has type %s while %s is expected.\n"), ..
      "benchfun", 2, "fun" , typeof(__benchfun_fun__) , "function"))
  end
  if ( typeof(iargs) <> "list" ) then 
    error(sprintf(gettext("%s: Wrong type for input argument #%d: variable %s has type %s while %s is expected.\n"), ..
      "benchfun", 3, "nlhs" , typeof(iargs) , "list"))
  end
  if ( typeof(nlhs) <> "constant" ) then 
    error(sprintf(gettext("%s: Wrong type for input argument #%d: variable %s has type %s while %s is expected.\n"), ..
      "benchfun", 4, "nlhs" , typeof(nlhs) , "constant"))
  end
  if ( typeof(kmax) <> "constant" ) then 
    error(sprintf(gettext("%s: Wrong type for input argument #%d: variable %s has type %s while %s is expected.\n"), ..
      "benchfun", 5, "kmax" , typeof(kmaxs) , "constant"))
  end
  //
  // Check size
  if ( size(name,"*") <> 1 ) then 
    error(sprintf(gettext("%s: Wrong size for input argument #%d: variable %s has size %d while %d is expected.\n"), ..
      "benchfun", 1, "name" , size(name,"*") , 1))
  end
  if ( size(nlhs,"*") <> 1 ) then 
    error(sprintf(gettext("%s: Wrong size for input argument #%d: variable %s has size %d while %d is expected.\n"), ..
      "benchfun", 4, "nlhs" , size(nlhs,"*") , 1))
  end
  if ( size(kmax,"*") <> 1 ) then 
    error(sprintf(gettext("%s: Wrong size for input argument #%d: variable %s has size %d while %d is expected.\n"), ..
      "benchfun", 5, "kmax" , size(kmax,"*") , 1))
  end

  //
  // Compute the intstruction string to be launched
  ni = length ( iargs )
  instr = ""
  // Put the LHS arguments
  if ( nlhs > 0 ) then
    instr = instr + "["
  end
  for i = 1 : nlhs
    if ( i > 1 ) then
      instr = instr + ","
    end
    instr = instr + "x" + string(i)
  end
  if ( nlhs > 0 ) then
    instr = instr + "]"
  end
  if ( nlhs > 0 ) then
    instr = instr + "="
  end
  // Put the RHS arguments
  instr = instr + "__benchfun_fun__("
  for i = 1 : ni
    if ( i > 1 ) then
      instr = instr + ","
    end
    instr = instr + "iargs("+string(i)+")"
  end
  instr = instr + ")"
  //
  // Loop over the tests
  for k = 1 : kmax
    // Call the function
    timer()
    ierr = execstr ( instr , "errcatch" )
    t(k) = timer()
    if ( ierr <> 0 ) then
      errmsg = lasterror()
      error(sprintf(gettext("%s: Failed to run function: %s.\n"), ..
        "benchfun", errmsg))
    end
  end
  msg = msprintf("%s: %d iterations, mean=%f, min=%f, max=%f\n",name,kmax,mean(t),min(t),max(t))
  mprintf("%s\n",msg)
endfunction

