// Copyright (C) 2009 - Michael Baudin
// This file must be used under the terms of the GNU LGPL license.

i=1:4
1-(5/6)^i
i=[10 20 24 25 30]
1-(35/36)^i

// Returns %T if one of the 4 die rolls is a 6.
function o = perform4rolls ()
  s = ceil(rand(4,1) * 6)
  is6 = s==6
  where6 = find(is6)
  o = where6<>[]
endfunction 


// Perform simulation
for n = [100 1000 10000 100000]
  for i = 1:n
    a(i) = perform4rolls();
  end
  wheretrue = find(a==%T);
  p = length(wheretrue)/n;
  mprintf("n=%d, p=%f\n",n,p);
  clear a;
end

// Perform vectorized simulation
// Returns the probability of success in n trials of 4 rolls
function p = perform4rollsNtimes ( n )
  s = ceil(rand(4,n) * 6);
  is6 = s==6;
  success = or(is6,"r");
  wheretrue = find(success==%T);
  p = length(wheretrue)/n;
endfunction 
// Perform simulation
for n = [1.e2 5.e2 1.e3 5.e3 1.e4 5.e4 1.e5]
  p = perform4rollsNtimes (n);
  mprintf("n=%d, p=%f\n",n,p);
end

// Returns %T if one of the k double die rolls is a double 6.
function o = performKDoublerolls (k)
  s = ceil(rand(2,k) * 6)
  is66 = and( s == 6 , "r" )
  where6 = find(is66)
  o = where6<>[]
endfunction 
// Perform simulation with k = 24 rolls
k=24
for n = [100 1000 10000 100000]
  for i = 1:n
    a(i) = performKDoublerolls ( k );
  end
  wheretrue = find(a==%T);
  p = length(wheretrue)/n;
  mprintf("n=%d, p=%f\n",n,p);
  clear a;
end

// Vectorize the computation
// Returns the probability of success in n trials of k rolls of a pair of dice.
function p = performKDoubleNtimes ( k , n )
  die1 = ceil(rand(n,k) * 6);
  die2 = ceil(rand(n,k) * 6);
  where66 = (die1==6) & (die2==6);
  success = or(where66,"c");
  wheretrue = find(success==%T);
  p = length(wheretrue)/n;
endfunction 
// Perform simulation with k = 24 rolls
k=24
for n = [1.e2 1.e3 1.e4 1.e5 5.e5]
  p = performKDoubleNtimes ( k , n );
  mprintf("n=%d, p=%f\n",n,p);
  clear a;
end

