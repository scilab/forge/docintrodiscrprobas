// Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


/////////////////////////////////////////////////////////////
// Pascal up matrix.
// Column by column version
function c = pascalup_col (n)
   c = eye(n,n)
   c(1,:) = ones(1,n)
   for i = 2:(n-1)
      c(2:i,i+1) = c(1:(i-1),i)+c(2:i,i)
   end
endfunction


benchfun ( "pascalup_col" , pascalup_col , list(100) , 1 , 10 );

///////////////////////////////////////////////////////////////
function F = subsets ( E , m )
   // Authors : Chancelier, Pincon
   // compute all the subsets with m elements of the set E
   // (E must be given as a row vector and all its elements
   // must be differents). For sets of numbers.
   n = length(E)
   if m > n then
      F = []
   elseif m == n
      F = E
   elseif m == 1
      F = E';
   else
      F = [];
      for i = 1:n-m+1
         EE = E(i+1:n)
         FF = subsets(EE,m-1)
         mm = size(FF,1)
         F = [F ; E(i)*ones(mm,1),FF]
      end
   end
endfunction

benchfun ( "subsets" , subsets , list(1:10,3) , 1 , 10 );

benchfun ( "sin" , sin , list(ones(100,100)) , 1 , 10 );

