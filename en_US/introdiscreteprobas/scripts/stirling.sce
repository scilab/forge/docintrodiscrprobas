// Copyright (C) 2009 - Michael Baudin
// This file must be used under the terms of the GNU LGPL license.

// Check Stirling's formula for large values of n
format(10)
n = (1:20:185)';
// Exact factorial
f = factorial(n);
// Stirlings formula
s = sqrt(2*%pi.*n).*(n./%e).^n;
// The number of significant digits
d = -log10(abs(f-s)./f);
[n f s d]

// Check formula for log(n!)
n = logspace(1,10,10)';
f = gammaln(n+1);
s = n.*log(n)-n;
// The number of significant digits
d = -log10(abs(f-s)./f);
[n f s d]


