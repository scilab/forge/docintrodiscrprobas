// Copyright (C) 2011 - DIGITEO - Michael Baudin
// This file must be used under the terms of the GNU LGPL license.


function lifeprint(ages, males, females)
    nc = size(ages,"*");
    mprintf(" <1     %6d %6d\n",males(1),females(1));
    for k = 2:nc
        amin = ages(k-1) + 1;
        amax = ages(k);
        mprintf("%3d-%3d %6d %6d\n",amin,amax,males(k),females(k));
    end
endfunction

function lifeprintLatex(ages, males, females)
    mprintf("<1 & %6d & %6d \\\\\n",males(1),females(1));
    for k = 2:nc
        amin = ages(k-1) + 1;
        amax = ages(k);
        mprintf("%3d-%3d & %6d & %6d \\\\\n",amin,amax,males(k),females(k));
    end
endfunction

function p = lifecondproba(a, b, ages, survivors)
    // Returns the probability that a person with age a 
    // can live to age b, given the datas in the tables ages and survivors.
    // We assume that a < b.
    // ages: a n-by-1 matrix of doubles, the age classes
    // survivors: a n-by-1 matrix of doubles, the number of survivors
    if ( a >= b ) then
        p = 1
        return
    end
    pa = lifeproba(a, ages, survivors)
    pb = lifeproba(b, ages, survivors)
    p = pb/pa
endfunction

function p = lifeproba(a, ages, survivors)
    // Returns the probability that a person can live to age a, 
    // given the datas in the tables ages and datas.
    // ages: a n-by-1 matrix of doubles, the age classes
    // survivors: a n-by-1 matrix of doubles, the number of survivors
    nc = size(ages,"*");
    for k = 2:nc
        if ( ages(k-1) + 1 <= a & a <= ages(k) ) then
            break
        end
    end
    if (k==nc) then
        error("Age not found in table")
    end
    p = survivors(k)/survivors(1)
endfunction

ages = [0;4;9;14;19;24;29;34;39;44;49;54;..
    59;64;69;74;79;84;89;94;99;100];
males = [100000;99276;99156;99085;98989;98573;..
    97887;97223;96526;95665;94396;92487;89643;85726;
    80364;72889;62860;49846;34096;18315;7198;1940];
females = [100000;99391;99292;99232;99164;98991;98758;..
    98484;98133;97621;96823;95603;93850;91384;
    87726;82275;74398;63218;48086;30289;14523;4804];
  
lifeprint(ages, males, females);

//
// For LaTeX formatting

// Compute the probability that a female lives to age 60.
pa = lifeproba(60, ages, females)
// Compute the probability that a female lives to age 80.
pb = lifeproba(80, ages, females)
// Compute the probability that a female lives to age 80, 
// given that she is 60.
pab = lifecondproba(60, 80, ages, females)


// Compute the probability that a female live to various ages, 
// given that she is 40.
bages = floor(linspace(41,99,20));
for k = 1 : 20
    pab(k) = lifecondproba(40, bages(k), ages, females);
end
plot(bages,pab,"bo-");
xtitle("Probability of living to age B, for a women of age 40.",..
    "Age B","Probability");

