// Copyright (C) 2009-2010 - Michael Baudin
// This file must be used under the terms of the GNU LGPL license.

// hygepdf --
//   Returns the probability of having x red balls when selecting n balls 
//   in an urn where there are m balls, where k balls are red.
// Caution !
//   This is a naive implementation !
function p = hygepdf_naive ( x , m , k , n )
  p = nchoosek(k,x) * nchoosek(m-k,n-x) / nchoosek(m,n)
endfunction

///
//// hygepdf evaluates the hypergeometric PDF.
//
//  Formula:
//
//    PDF(x)(n,k,m) = C(k,x) * C(m-k,n-x) / C(m,n).
//
//  Definition:
//
//    PDF(x)(n,k,m) is the probability of drawing x white balls in a
//    single random sample of size n from a population containing
//    k white balls and a total of m balls.
//
//  Modified:
//
//    12 September 2004
//
//  Author:
//
//    John Burkardt
//    Scilab version and update : Michael Baudin
//
//  Parameters:
//
//    Input, integer x, the desired number of white balls. 0 <= x <= n, usually, although any value of x can be given.
//
//    Input, integer n, the number of balls selected. 0 <= n <= m.
//
//    Input, integer k, the number of white balls in the population. 0 <= k <= m.
//
//    Input, integer m, the number of balls to select from. 0 <= m.
//
//    Output, real PDF, the probability of exactly x white balls.
//
function p = hygepdf ( x, m, k, n )
//
//  Special cases.
//
  if ( x < 0 ) then
    p = 1.0
  elseif ( n < x ) then
    p = 0.0
  elseif ( k < x ) then
    p = 0.0
  elseif ( m < x ) then
    p = 0.0
  elseif ( n == 0 ) then
    if ( x == 0 ) then
      p = 1.0
    else
      p = 0.0
    end
  else
    c1 = nchooseklog (   k,   x )
    c2 = nchooseklog ( m-k, n-x )
    c3 = nchooseklog ( m,   n   )
    p_log = c1 + c2 - c3
    p = exp ( p_log )
  end
endfunction

