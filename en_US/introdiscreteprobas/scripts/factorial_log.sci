// Copyright (C) 2009 - Michael Baudin


//// FACTORIAL_LOG returns the logarithm of N!.
//
//  Definition:
//
//    N! = Product ( 1 <= I <= N ) I
//
//  Method:
//
//    N! = Gamma(N+1).
//
//  Modified:
//
//    11 September 2004
//
//  Author:
//
//    John Burkardt
//    Scilab version : Michael Baudin
//
//  Parameters:
//
//    Input, integer N, the argument of the function.
//    0 <= N.
//
//    Output, real FACTORIAL_LOG, the logarithm of N!.
//
// Drawback: does not accept matrices of integers.
function flog = factoriallog_naive1 ( n )
  flog = sum(log(2:n));
endfunction

// Another implementation, based on gammaln
// Drawback: does not accept hypermatrices
function flog = factoriallog ( n )
  flog = gammaln(n+1);
endfunction

// Another implementation
// Good point: it accepts hypermatrices.
// Drawback : more memory than necessary
function flog = factoriallog_naive ( n )
  n(n==0)=1
  t = cumsum(log(1:max(n)))
  v = t(n)
  flog = matrix(v,size(n))
endfunction

// Draw the logarithm of the factorial number
x = linspace ( 0 , 170 , 1000 );
y = feval ( x , factoriallog );
plot ( x , y )
f=gcf();
f.children.title.text="Logarithm of n!";
f.children.x_label.text="n";
f.children.y_label.text="log(n!)";

// compare the three approaches
n = linspace ( 0 , 100 , 11 )';
[n factoriallog(n) factoriallog_naive(n)]


