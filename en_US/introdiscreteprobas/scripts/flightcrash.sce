// Copyright (C) 2009 - Michael Baudin
// This file must be used under the terms of the GNU LGPL license.



// The number of flights per day
NBF = 20000

// Probability of getting exactly 5 crash in 22*NBF flights
// where each flight has a crash probability equal to p = 1/500 000
// pc = 0.0018241
p = 1/500000
q = 1-p
n = 22*NBF
j = 5
pc = nchoosek(n,j) * p^j * q^(n-j)

// pcrash --
//   Returns the probability of having j crash in n flights
function pc = pcrash ( n , j )
  p = 1/500000
  q = 1-p
  pc = nchoosek(n,j) * p^j * q^(n-j)
endfunction

// Table des probabilitÚs pour j crash en 22 jours
pc = zeros(11)
for j = 0:10
  pc(j+1) = pcrash ( 22*NBF , j );
  //mprintf("P(%d crash in 22 days)=%1.8f ~ 1/%d\n",j,pc(j+1),int(1/pc(j+1)));
  mprintf("%d crash in 22 days & %1.8f\\\\\n",j,pc(j+1));
end

// P(at least 5 crash in 22 days)= 1 - P(between 0 and 4 crash in 22 days) 
// = 0.0021294
1- sum(pc(1:5))

// Table of probabilities for j crash in 6 weeks = 6*7=42 days
pc = zeros(11)
for j = 0:10
  pc(j+1) = pcrash ( 42*NBF , j );
  //mprintf("P(%d crash in 42 days)=%1.8f ~ 1/%d\n",j,pc(j+1),int(1/pc(j+1)));
  mprintf("%d crash in 42 days & %1.8f\\\\\n",j,pc(j+1));
end
// Probability of exactly 3 crash in 42 days = 0.0610235
pcrash ( 42*NBF , 3 )
// P(at least 3 crash in 42 days)= 1 - P(between 0 and 2 crash in 42 days) 
// = 0.2375067
1- sum(pc(1:3))

// Probability "no crash in 1 day"
// 0.6703174
( 49999 / 50000)^NBF

// Probability "at least one crash in 1 day"
// 0.3296826
1. - ( 49999 / 50000)^NBF

// Probability of exactly 5 crash in 22 days
// Unit = day = NBF flights
n = 22
j = 5
p = 1- (499999/500000)^NBF
pc = nchoosek(n,5)*p^j * (1-p)^(n-j) // 0.0012366 

// Probability of exactly 5 crash in 22 days
// Unit = 1/2 day = NBF/2 flights
n = 22
j = 5
p = 1- (499999/500000)^(NBF/2)
pc = nchoosek(n*2,5)*p^j * (1-p)^(n*2-j) // 0.1288264

// Probability of exactly 5 crash in 22 days
// Unit =  1h = NBF/24 flights
n = 22
j = 5
p = 1- (499999/500000)^(NBF/24)
pc = nchoosek(n*24,5)*p^j * (1-p)^(n*24-j) // 0.1329871 


// Probability of exactly 5 crash in 22 days
// Approximation by Poisson
p = 1/500000
n = 22*NBF
j = 5
lambda = n * p
p = 1 / 500000
pc = lambda^j * exp(-lambda) / factorial(j) // 0.1332955

// Probability of having at least 5 crash in one period of 22 days
patleast = 0.0021294
// Probability of having less than 4 crash in one period of 22 days
// 0.9978706
p4 = 1 - patleast
// Probability of having less than 4 crash in 16 periods
// 0.9664684
pyear = p4^16
// Probability of having one period which contains at least 5 crash
// 0.0335316
pyear5 = 1 - pyear

