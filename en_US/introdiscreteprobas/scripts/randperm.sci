// Copyright (C) 2009 - 2010 - Michael Baudin

// randperm  Random permutation.
//
// p = randperm(n) returns a random permutation of 1:n.
// Side effect : modifies the state of the grand random number generator.
// This is funny, but not as efficient as using the "prm" option of grand.
// This Scilab function is a port of 
// http://web.mit.edu/18.06/www/Course-Info/Mfiles/randperm.m
// which is headed by Gilbert Strang at MIT.
function p = randperm_weird ( n )
  [ignore, p] = gsort(grand(1,n,"def")',"r","i")
endfunction

// See, for example : 
// http://bugzilla.scilab.org/show_bug.cgi?id=5921
// Side effect : modifies the state of the grand random number generator.
function p = randperm ( n )
  p = grand(1,"prm",(1:n)')
endfunction


randperm_weird ( 10 )'
randperm_weird ( 10 )'
randperm_weird ( 10 )'

randperm ( 10 )'
randperm ( 10 )'
randperm ( 10 )'

// Randomly shuffles the given array x.
function x = genprm ( x )
  n = size(x,"*")
  for i = 1:n
    // Get a random number k between i and n.
    t = grand(1,1,"unf",0,1)
    k = floor ( t * (n - i + 1)) + i
    // Exchange the elements k and i.
    elt = x(k)
    x(k) = x(i)
    x(i) = elt
  end
endfunction

genprm ( 1:10 )
genprm ( 1:10 )
genprm ( 1:10 )

