// Copyright (C) 2009 - 2010 - Michael Baudin

// Simulate the prediction of seisms by random chance.

// Returns a ( nbrows x nbcols ) matrix of integers in the range 1,m
function ri = generateInRange1M ( m , nbrows , nbcols )
  ri = floor ( rand ( nbrows , nbcols ) * m) + 1
endfunction
r = generateInRange1M ( 5 , 100 , 100 );
 counter = zeros (1 ,5);
for i = 1:100
for j = 1:100
 k = r(i,j);
 counter (k) = counter (k) + 1;
end
end
counter = counter / 10000;
counter
bar( counter )

// simulate --
//   Returns a boolean array of n entries with 
//   d true, where n is the total number of
//     simulated days and d is the number of days 
//     with a seism.
function outcome = simulate ( n , d , verbose )
  // By default, there is no seism
  taken(1 : n) = %f
  outcome(1 : d) = 0
  //  pick the days of seism at random
  for i = 1 : d
    run = %t
    while ( run )
      j = generateInRange1M ( n , 1 , 1 )
      if ( verbose == %t ) then
      mprintf("Picking day #%d/%d, taken : %s\n",j,n,string(taken(j)==%t))
      end
      if ( taken(j) == %f ) then
        outcome(i) = j
        taken(j) = %t
        run = %f
      end
    end
  end
endfunction

// Number of days in the experiment : n
n = 200;
// Number of actual seism days in the experiment : d
d = 50;
// Number of predictions in the experiment : k
k = 100;
outcome = simulate ( n , d , %f );
// Must be 50
size(outcome)

actual = simulate ( n , d , %f );
predict = simulate ( n , k , %f );

// compare --
//   Returns the array of correct days, where 
//   the prediction matches the actual seisms
function correct = compare ( actual , predict , verbose )
// Search how many good predictions are correct
good = 0;
correct = [];
for i = 1 : size(predict,"*")
  f = find(actual==predict(i));
  if ( f<>[] ) then
    if ( verbose == %t ) then
    mprintf("Day = %d is correct\n",predict(i))
    end
    good = good + 1;
    correct($+1) = predict(i);
  else
    if ( verbose == %t ) then
    mprintf("Day = %d is not correct\n",predict(i))
    end
  end
end
endfunction

correct = compare ( actual , predict , %t );
mprintf("Number of correct predictions=%d\n",size(correct,"r"))
mprintf("Correct predictions:\n")
correct

// Expected = [3 7]
correct = compare ( [1 3 5 7 9] , [3 7 4 8] , %f );

// Make several experiments
numbercorrect = [];
for i  = 1 : 100
  actual = simulate ( n , d , %f );
  predict = simulate ( n , k , %f );
  correct = compare ( actual , predict , %f );
  mprintf("Number of correct predictions=%d\n",size(correct,"r"));
  numbercorrect($+1) = size(correct,"r");
end
// Display summary statistics
mprintf("Min:%d\n",min(numbercorrect));
mprintf("Max:%d\n",max(numbercorrect));
mprintf("Mean:%d\n",mean(numbercorrect));
mprintf("Var:%d\n",variance(numbercorrect));

// Display expectations for the mean and the variance
mprintf("Expected mean:%d\n",k*d/n);
mprintf("Expected var:%d\n",k*d*(n-k)*(n-d)/n^2/(n-1));
mprintf("Expected mode:%d\n",(k+1)*(d+1)/(n+2));


